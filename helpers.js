//image directory
const imageUploadPath = path.join(__dirname, process.env.UPLOADS_DIR);
async function processAndSaveImage(uploadedImage) {
    // Creamos el directorio (con recursive: true por si hay subdirectorios y así no da error)
    await fs.mkdir(imageUploadPath, { recursive: true });
  
    // Leer la imagen que se subio
    const image = sharp(uploadedImage.data);
  
    // Saco información de la imagen
    const imageInfo = await image.metadata();
  
    // Cambiarle el tamaño si es necesario
    if (imageInfo.width > 1000) {
      image.resize(1000);
    }
  
    // Guardar la imagen en el directorio de subidas
    const imageFileName = `${uuid.v4()}.jpg`;
    await image.toFile(path.join(imageUploadPath, imageFileName));
  
    // Devolver el nombre con el que fue guardada
    return imageFileName;
  }
  

function generateError(message, code = 500) {
    const error = new Error(message);
    error.httpStatus = code;
    return error;
  }

  function showDebug(message) {
    if (process.env.NODE_ENV === "development") {
      console.log(message);
    }
  }
  

  
  module.exports = {
    processAndSaveImage,
    deleteUpload,
    generateError,
    showDebug,
  };
  