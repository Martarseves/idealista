const { getConnection } = require("../db");

async function adList(req,res,next){
    let connection;
    try{
        connection=await getConnection();
        const {order, direction } = req.query;

        const orderDirection=
        (direction && direction.toLowerCase())=== "DESC";

        let orderBy;
        switch(order){
            case "adRating":
                orderBy="adRating";
                break;
        }

        let queryResults;

        queryResults = await connection.query(
            `
            SELECT ads.id, ads.rating, 
            (SELECT AVG(rating) FROM ads WHERE ads_id=ads.id) AS voteAverage
            FROM ads WHERE ads.ratings>40
            ORDER BY ${orderBy} ${orderDirection}`
          );
    
        const [result]=queryResults;

        res.send({
            status:"ok",
            data:result,
        });
    } catch(error){
        next(error);
    } finally{
        if(connection) connection.release();
    }
}

module.exports=adList;
