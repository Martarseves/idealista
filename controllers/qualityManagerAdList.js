const { getConnection } = require("../db");
const {isQualityManager}=require("../middlewares/isQualityManager");
//requerimos de jwt para poder hacer una validación correcta del tipo de rol del usuario
//yo haría un Login y unas validaciones a través de Schemas pero no se encuentra en las especificaciones


async function qualityManagerAdList (req,res,next){
    let connection;
    try{
        connection=await getConnection();
        const { order, direction } = req.query;

        const orderDirection=
        (direction && direction.toLowerCase())=== "desc" ? "DESC" : "ASC";

        let orderBy;
        switch(order){
            case "adRating":
                orderBy="adRating";
                break;
            case "date":
                orderBy="date";
        }

        let queryResults;

        queryResults = await connection.query(
            `
            SELECT ads.id, ads.rating, ads.date, 
            (SELECT AVG(rating) FROM ads WHERE ads_id=ads.id) AS voteAverage
            FROM ads 
            ORDER BY ${orderBy} ${orderDirection}`
          );

        const [result]=queryResults;

        res.send({
            status:"ok",
            data:result,
        });
    } catch(error){
        next(error);
    } finally{
        if(connection) connection.release();
    }
}

module.exports=qualityManagerAdList;
