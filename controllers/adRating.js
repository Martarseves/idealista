const { getConnection } = require("../db");

const {rateCalculator}=require("../services/rateCalculator");

async function adRating (req,res,next){
    let connection;
    try{
        connection=await getConnection();
        
        rateCalculator();

        let queryResults;

        queryResults = await connection.query(
            `
            SELECT ads.id, ad.rating
            FROM ads 
            `
          );

        const [result]=queryResults;

        res.send({
            status:"ok",
            data:result,
        });
    } catch(error){
        next(error);
    } finally{
        if(connection) connection.release();
    }
}

module.exports=adRating;