require("dotenv").config();

const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const fileUpload = require("express-fileupload");
const path = require("path");

const app = express();

//MIDDLEWARES
const adExists=require("./middlewares/adExists");
const isQualityManager=require("./middlewares/isQualityManager");


//CONTROLLERS
const adList=require("./controllers/adList");
const qualityManagerAdList=require("./controllers/qualityManagerAdList");
const adRating=require("./controllers/adRating");


//ENDPOINTS
//list ads for regular users
app.get("/userads", adList);
//list ads for quality manager
app.get("/qm-ads",isQualityManager, qualityManagerAdList);
//rating ads
app.put("adrating", adExists, adRating)


// Error middleware
app.use((error, req, res, next) => {
    console.error(error);
  
    res.status(error.httpStatus || 500).send({
      status: "error",
      message: error.message,
    });
  });
  
  // Not found
  app.use((req, res) => {
    res.status(404).send({
      status: "error",
      message: "Not found",
    });
  });
  
  const port = process.env.PORT;
  
  app.listen(port, () => {
    console.log(`Server working on http://localhost:${port} :-)`);
  });
  


