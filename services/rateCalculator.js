    async function rateCalculator(advertisement, ads_photos, ads_features){
        let rating=0;
        let complete=true;

        const advertisementDescription=advertisement.description;

        //description points
       if (advertisementDescription!==null && advertisementDescription.length<20) {
           rating=rating+0;
       } else if(advertisementDescription.length<50){
           rating=rating+10;
       } else{
           rating=rating+30;
       }

       //feature points
       ads_features.forEach(feature => {
           if(feature.name==="Atico"){
            rating=rating+5;
           }   
           if(feature.name==="Luminoso"){
            rating=rating+5;
           }
           if(feature.name==="Centrico"){
            rating=rating+5;
           }
           if(feature.name==="Reformado"){
            rating=rating+5;
           }
           if(feature.name==="Nuevo"){
            rating=rating+5;
           }     
       });

       //photo points
       ads_photos.forEach(photo => {
        if(photo!==null){
         rating=rating+10;
        } else {
            rating=rating-10;
        }
        if(photo.hd===1){
         rating=rating+20;
        }
        });

        //completed ad points
       if (advertisementDescription===null && advertisement.type!=="Garaje"){
         complete=false;
       } 
       if(advertisement.type==="Chalet" && advertisement.gardensize!==null && advertisement.size!==null){
           complete=false;
       } 
       if(advertisement.type==="Piso" && advertisement.size!==null){
        complete=false;
       } 
      
    }

    module.exports = rateCalculator;