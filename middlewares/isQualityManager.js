async function isQualityManager(req, res, next) {
    if (req.auth.role === "qualitymanager") {
      next();
    } else {
      const error = new Error("You don't have quelity manager permissions");
      error.httpStatus = 403;
      next(error);
    }
  }
  
  module.exports = isQualityManager;
  