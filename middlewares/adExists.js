const { getConnection } = require("../db");
const { generateError } = require("../helpers");

async function adExists(req, res, next) {
  let connection;
  try {
    connection = await getConnection();
    const { id } = req.params;

    const [current] = await connection.query(
      `
    SELECT id
    FROM ads
    WHERE id=?
  `,
      [id]
    );

    if (current.length === 0) {
      throw generateError(
        `Ad with id ${id} doesn't exists in database`,
        404
      );
    } else {
      next();
    }
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = adExists;
