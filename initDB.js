require("dotenv").config();

const { formatDateToDB } = require("./helpers");
const {getConnection}=require("./db");

let connection;

async function main(){
    try{
    connection=await getConnection();

    await connection.query("DROP TABLE IF EXISTS users");
    await connection.query("DROP TABLE IF EXISTS ads");  

    await connection.query(`
    CREATE TABLE users(
        id INTEGER PRIMARY KEY AUTO_INCREMENT,
        rol ENUM("qualitymanager","user")        
    ); 
    `);

    await connection.query(`
    CREATE TABLE ads(
        id INTEGER PRIMARY KEY AUTO_INCREMENT,
        description VARCHAR(255) DEFAULT NULL,
        type ENUM("Piso","Chalet","Garaje"),
        size DECIMAL(8,2),
        gardensize DECIMAL(10,2),
        rating INT DEFAULT NULL,
        date DATETIME
    );
    `);

    await connection.query(`
    CREATE TABLE ads_photos(
        id INTEGER PRIMARY KEY AUTO_INCREMENT,
        hd BOOLEAN DEFAULT 0,
        photo TINYTEXT,
        ad_id INTEGER NOT NULL,
    );
    `);

    await connection.query(`
    CREATE TABLE ads_features(
        id INTEGER PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(45);
        ad_id INTEGER NOT NULL,
    );
    `)

} catch(error){
    console.error(error);
} finally{
    if(connection) connection.release();
    process.exit();
}
}

main();